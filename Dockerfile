FROM node:18

ENV ANDROID_HOME=/android
ENV PATH=$ANDROID_HOME/cmdline-tools/tools/bin/:$ANDROID_HOME/emulator/:$ANDROID_HOME/platform-tools/:$PATH

RUN apt-get update \
  && apt-get install -y \
    openjdk-17-jdk-headless \
  && mkdir /android \
  && cd /android \
  && wget "https://dl.google.com/android/repository/commandlinetools-linux-9123335_latest.zip" \
  && unzip commandlinetools-linux-9123335_latest.zip \
  && rm -f commandlinetools-linux-9123335_latest.zip \
  && mkdir tools \
  && mv cmdline-tools/* tools/ \
  && mv tools cmdline-tools/ \
  && yes | /android/cmdline-tools/tools/bin/sdkmanager --install "platform-tools" "platforms;android-30" "build-tools;30.0.1" "emulator"
